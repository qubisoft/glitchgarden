﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class OptionsController : MonoBehaviour
{

    [SerializeField] Slider volumeSlider;
    [SerializeField] float defaultVolume = 0.8f;

    [SerializeField] Slider difficultSlider;
    [SerializeField] int defaultDifficult = 1;


    // Start is called before the first frame update
    void Start()
    {
        volumeSlider.value = PlayerPrefsController.GetMasterVolume();
        difficultSlider.value = PlayerPrefsController.GetMasterDifficulty();
    }

    // Update is called once per frame
    void Update()
    {
        var musicPlayer = FindObjectOfType<MusicPlayer>();
        if (musicPlayer)
        {
            musicPlayer.SetVolume(volumeSlider.value);
        }
        else
        {
            Debug.LogWarning("no music player found");
        }

        //var dificultLevels = FindObjectOfType<DifficultLevels>();
        //if (dificultLevels)
        //{
        //    dificultLevels.SetDifficult(difficultSlider.value);
        //}
        //else
        //{
        //    Debug.LogWarning("no diff level found");
        //}
        
    }
    

    public void SaveAndLoadMainMenu()
    {
        PlayerPrefsController.SetMasterVolume(volumeSlider.value);
        PlayerPrefsController.SetMasterDifficulty(Mathf.FloorToInt(difficultSlider.value));
        SceneManager.LoadScene("Start Screen");
    }

    public void SaveToDefault()
    {
        volumeSlider.value = defaultVolume;
        difficultSlider.value = defaultDifficult;
    }

}
