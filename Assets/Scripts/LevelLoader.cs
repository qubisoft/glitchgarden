﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] int TimeToLoad = 3;
    int currentSceneIndex;

    private void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0)
        {
            StartCoroutine(WaitForTime());
        }
    }

    //public void LoadSplashScreen()
    //{
    //    StartCoroutine(StartGameScene(TimeToLoad));        
    //}

    //private IEnumerator StartGameScene(int timeToLoadStartScene)
    //{
    //    yield return new WaitForSeconds(TimeToLoad);
    //    SceneManager.LoadScene("Splash Screen");        
    //}

    public void QuitGame()
    {
        Application.Quit();
    }


    IEnumerator WaitForTime()
    {
        yield return new WaitForSeconds(TimeToLoad);
        LoadNextScene();
    }
       
    public void LoadNextScene()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene + 1);
    }

    public void RestartScene()
    {
        Time.timeScale = 1;
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene);
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Start Screen");
    }

    public void LoadGameOverScene()
    {
        
        SceneManager.LoadScene("GameOver Screen");
    }
    public void PlayAgain()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Level 1");
    }
    public void LoadOprionsScene()
    {
        SceneManager.LoadScene("Options");
    }


}
