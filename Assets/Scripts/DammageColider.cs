﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DammageColider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        FindObjectOfType<LiveDisplay>().takeLive(1);
        Destroy(collision.gameObject);
    }
}
