﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{

    [SerializeField] GameObject projectile, gun;
    AttackerSpawner myLaneSpawner;
    Animator animator;

    GameObject projectileParent;
    const string PROJECTILE_PARENT_STRING = "Projectiles";

    private void Start()
    {
        CreateProjectileParent();

        SetLaneSpawner();
        animator = GetComponent<Animator>();
    }

    private void CreateProjectileParent()
    {
        projectileParent = GameObject.Find(PROJECTILE_PARENT_STRING);
        if (!projectileParent)
        {
            projectileParent = new GameObject(PROJECTILE_PARENT_STRING);
        }
    }

    private void SetLaneSpawner()
    {
        var spawners = FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner spawner in spawners)
        {
            bool IsCloseEnought = (Mathf.Abs(spawner.transform.position.y - transform.position.y) <= Mathf.Epsilon);
            if (IsCloseEnought)
            {
                myLaneSpawner = spawner;
            }

        }
    }

    private void Update()
    {
        if (IsAttackerInLine())
        {
            animator.SetBool("IsAttacking", true);            
        }
        else
        {
            animator.SetBool("IsAttacking", false);
        }
    }

    private bool IsAttackerInLine()
    {
        if (myLaneSpawner.transform.childCount <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public void Fire()
    {        
       GameObject _projectile = Instantiate(projectile, gun.transform.position, transform.rotation);
        _projectile.transform.parent = projectileParent.transform;
    }
}
