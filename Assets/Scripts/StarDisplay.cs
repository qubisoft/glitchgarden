﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarDisplay : MonoBehaviour
{
    [SerializeField] int stars = 100;
    Text starText;


    // Start is called before the first frame update
    void Start()
    {
        starText = GetComponent<Text>();
        updateDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void updateDisplay()
    {
        starText.text = stars.ToString();
    }

    public void AddStars(int val)
    {
        stars += val;
        updateDisplay();
    }

    public void SpendStars(int val)
    {
        stars -= val;
        updateDisplay();
    }

    public int GetCurrentStarValue()
    {
        return stars;
    }
}
