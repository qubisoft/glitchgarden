﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [SerializeField] ParticleSystem explosion;

    GameObject currenTarget;
    Animator animator;

    [Range(0f, 5f)]
    float currentSpeed = 1f;

    int health = 100;

    private void Awake()
    {
        FindObjectOfType<LevelControler>().AttackerSpawned();
    }

    private void OnDestroy()
    {
        LevelControler levelControler = FindObjectOfType<LevelControler>();
        if (levelControler != null)
        {
            levelControler.AttackerKilled();
        }
    }

    private void Start()
    {
        animator = GetComponent<Animator>();

        if (gameObject.GetComponent<Health>())
        {
            health = gameObject.GetComponent<Health>().getHealth();
        }
        else
        {
            return;
        }
        
    }

    void Update()
    {
        transform.Translate(Vector2.left * currentSpeed * Time.deltaTime);
        UpdateAnimationState();
    }

    private void UpdateAnimationState()
    {
        if (!currenTarget)
        {
            animator.SetBool("Attacking", false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) { return; };

        health -= damageDealer.GetDamage();

        damageDealer.Hit();

        if (health <= 0)
        {
            destroyAtaccker();
        }
    }

    public void destroyAtaccker()
    {
        ParticleSystem expl = Instantiate(explosion, transform.position, transform.rotation);
        Destroy(expl, 1f);

        Destroy(gameObject);
    }


    public void SetMovementSpeed(float speed)
    {
        currentSpeed = speed;
    }

    public void Attack(GameObject target)
    {        
        animator.SetBool("Attacking", true);
        currenTarget = target;
    }

    public void StrikeCurrentTarget(int damage)
    {
        if (!currenTarget) { return; }
        Health health = currenTarget.GetComponent<Health>();
        if (health)
        {
            health.DealDamage(damage);
        }
    }
}
