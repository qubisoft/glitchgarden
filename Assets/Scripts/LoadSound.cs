﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SingletonSetUp();
    }

    private void SingletonSetUp()
    {
        if (FindObjectsOfType<LoadSound>().Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
