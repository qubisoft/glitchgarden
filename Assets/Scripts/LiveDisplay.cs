﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LiveDisplay : MonoBehaviour
{
    Text liveDisplay;
    [SerializeField] int baseLives = 3;
    int live;



    // Start is called before the first frame update
    void Start()
    {
        liveDisplay = GetComponent<Text>();
        live = baseLives - PlayerPrefsController.GetMasterDifficulty();   
        
        var i = PlayerPrefsController.GetMasterDifficulty();

        Debug.LogWarning(" PlayerPrefsController.GetMasterDifficulty(); " + i);
    }

    // Update is called once per frame
    void Update()
    {
        liveDisplay.text = live.ToString();
        
        if(live <= 0)
        {
            GameOverDisplay();
        }
    }

    private void GameOverDisplay()
    {
        FindObjectOfType<LevelControler>().HandleLoseCondition();
    }

    public void AddLive(int val)
    {
        live += val;
    }
    public void takeLive(int val)
    {
        live -= val;
    }   
}
