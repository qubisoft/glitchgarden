﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour
{

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
                
        GameObject otherObj = collision.gameObject;
        if (otherObj.GetComponent<Defender>() != null)
        {
            if(otherObj.name == "Gravestone")
            {
                animator.SetTrigger("jumpTrigger");
                
            }
            else
            {
                gameObject.GetComponent<Attacker>().Attack(otherObj);
            }            
        }
    }
}
