﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lizard : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObj = collision.gameObject;
        if(otherObj.GetComponent<Defender>() != null)
        {
            gameObject.GetComponent<Attacker>().Attack(otherObj);
        }
    }
}
