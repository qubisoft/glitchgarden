﻿//using System;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour
{
    [SerializeField] bool startSpawn = true;
    [SerializeField] Attacker[] attackerPrefabArray;
    [SerializeField] float atackerSpeed;

    [SerializeField] int minSpawn = 0;
    [SerializeField] int maxSpawn = 5;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (startSpawn)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(minSpawn, maxSpawn));
            SpawnAttacker();            
        }
              
    }

    private void SpawnAttacker()
    {
        var attackerIndex = UnityEngine.Random.Range(0, attackerPrefabArray.Length);
        Spawn(attackerPrefabArray[attackerIndex]);


    }

    private void Spawn (Attacker myAttacker)
    {
        Attacker newAttacker = Instantiate(myAttacker, transform.position, transform.rotation) as Attacker;
        newAttacker.transform.parent = transform;
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void stopSpawning()
    {
        startSpawn = false;
    }
}
