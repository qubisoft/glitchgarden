﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultLevels : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        var level = PlayerPrefsController.GetMasterDifficulty();
        SetLevel(level);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDifficult(float value)
    {
        var level = Mathf.FloorToInt(value);
        SetLevel(level);
    }

    private void SetLevel(int level)
    {
        if(level == 1)
        {
            FindObjectOfType<LiveDisplay>().AddLive(10);
        }
        if (level == 2)
        {
            FindObjectOfType<LiveDisplay>().AddLive(20);
        }
        if (level == 3)
        {
            FindObjectOfType<LiveDisplay>().AddLive(30);
        }
    }
}
